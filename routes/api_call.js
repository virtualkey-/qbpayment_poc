var tools = require('../tools/tools.js')
var config = require('../config.json')
var request = require('request')
var express = require('express')
var router = express.Router()
var uuidv5 = require('uuid/v5');
/** /api_call **/
router.get('/createTokenForCard', function (req, res) {
 


  
  var token = tools.getToken(req.session)
  if(!token) return res.json({error: 'Not authorized'})
var options = { method: 'POST',
  url: config.api_uri+'/quickbooks/v4/payments/tokens',
  headers: 
   { 
    'Authorization': 'Bearer ' + token.accessToken,
     'Accept': 'application/json',
     'Content-Type': 'application/json' },
  body: 
   { card: 
      { expYear: '2020',
        expMonth: '02',
        address: 
         { region: 'CA',
           postalCode: '94086',
           streetAddress: '1130 Kifer Rd',
           country: 'US',
           city: 'Sunnyvale' },
        name: 'emulate=0',
        cvc: '123',
        number: '4111111111111111' } },
  json: true };

request(options, function (error, response, body) {
  if (error) 
  return error;

  return body;
  console.log(body);
});








})




/** /api_call **/
router.get('/createCardFromToken', function (req, res) {
  console.log("Inside createCardFromToken" );
  var token = tools.getToken(req.session)
  if(!token) return res.json({error: 'Not authorized'})
  if(!req.session.realmId) return res.json({
    error: 'No realm ID.  QBO calls only work if the accounting scope was passed!'
  })

  // Set up API call (with OAuth2 accessToken)
  var url = config.api_uri + '/quickbooks/v4/customers/606/cards'
  //req.session.realmId + '/companyinfo/' + req.session.realmId
  console.log('Making API call to: ' + url)
  var requestObj = {
    url: url,
    method: 'POST',
    headers: {
      'Authorization': 'Bearer ' + token.accessToken,
      'Accept': 'application/json',
      'Request-Id':uuidv5('hello world', '9f282611-e0fd-5650-8953-89c8e342da0b'),  // ⇨ '9f282611-e0fd-5650-8953-89c8e342da0b',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        "number": "4408041234567893",
        "expMonth": "12",
        "expYear": "2026",
        "name": "Test User",
        "address": {
            "streetAddress": "1245 Hana Rd",
            "city": "Richmond",
            "region": "VA",
            "country": "US",
            "postalCode": "44112"
         }
    })
  }

  // Make API call
  request(requestObj, function (err, response) {
    // Check if 401 response was returned - refresh tokens if so!
    tools.checkForUnauthorized(req, requestObj, err, response).then(function ({err, response}) {
      if(err || (response.statusCode != 201 && response.statusCode != 200)) {
        console.log("Inside error block while adding card" + err);
        console.log("Inside error block while adding card" + response.statusCode);
        
        return res.json({error: err, statusCode: response.statusCode})
      }

      console.log(res.json(JSON.parse(response.body)))
      // API Call was a success!
      res.json(JSON.parse(response.body))
    }, function (err) {
      console.log(err)
      return res.json(err)
    })
  })
})



/** /api_call **/
router.get('/createChargeOnCard', function (req, res) {
  var token = tools.getToken(req.session)
  if(!token) return res.json({error: 'Not authorized'})
  if(!req.session.realmId) return res.json({
    error: 'No realm ID.  QBO calls only work if the accounting scope was passed!'
  })

  // Set up API call (with OAuth2 accessToken)
  var url = config.api_uri + '/quickbooks/v4/customers/605/cards'
  //req.session.realmId + '/companyinfo/' + req.session.realmId
  console.log('Making API call to: ' + url)
  var requestObj = {
    url: url,
    method: 'POST',
    headers: {
      'Authorization': 'Bearer ' + token.accessToken,
      'Accept': 'application/json',
      'Request-Id': 'frtg55674-7588-3333333-82d9-bc721c3c0f55',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        "number": "4408041234567893",
        "expMonth": "12",
        "expYear": "2026",
        "name": "Test User",
        "address": {
            "streetAddress": "1245 Hana Rd",
            "city": "Richmond",
            "region": "VA",
            "country": "US",
            "postalCode": "44112"
         }
    })
  }

  // Make API call
  request(requestObj, function (err, response) {
    // Check if 401 response was returned - refresh tokens if so!
    tools.checkForUnauthorized(req, requestObj, err, response).then(function ({err, response}) {
      if(err || (response.statusCode != 201 && response.statusCode != 200)) {
        console.log("Inside error block while adding card" + err);
        console.log("Inside error block while adding card" + response.statusCode);
        
        return res.json({error: err, statusCode: response.statusCode})
      }

      console.log(res.json(JSON.parse(response.body)))
      // API Call was a success!
      res.json(JSON.parse(response.body))
    }, function (err) {
      console.log(err)
      return res.json(err)
    })
  })
})



/** /api_call/revoke **/
router.get('/revoke', function (req, res) {
  var token = tools.getToken(req.session)
  if(!token) return res.json({error: 'Not authorized'})

  var url = tools.revoke_uri
  request({
    url: url,
    method: 'POST',
    headers: {
      'Authorization': 'Basic ' + tools.basicAuth,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      'token': token.accessToken
    })
  }, function (err, response, body) {
    if(err || response.statusCode != 200 || response.statusCode != 201) {
      return res.json({error: err, statusCode: response.statusCode})
    }
    tools.clearToken(req.session)
    res.json({response: "Revoke successful"})
  })
})

/** /api_call/refresh **/
// Note: typical use case would be to refresh the tokens internally (not an API call)
// We recommend refreshing upon receiving a 401 Unauthorized response from Intuit.
// A working example of this can be seen above: `/api_call`
router.get('/refresh', function (req, res) {
  var token = tools.getToken(req.session)
  if(!token) return res.json({error: 'Not authorized'})

  tools.refreshTokens(req.session).then(function(newToken) {
    // We have new tokens!
    res.json({
      accessToken: newToken.accessToken,
      refreshToken: newToken.refreshToken
    })
  }, function(err) {
    // Did we try to call refresh on an old token?
    console.log(err)
    res.json(err)
  })
})

module.exports = router
